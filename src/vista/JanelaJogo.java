package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import pt.candledei.Jogo;
import pt.candledei.PainelBonus;
import pt.candledei.PainelPrincipal;
import pt.candledei.PainelVida;
import pt.ipleiria.estg.dei.gridpanel.GridPanel;

public class JanelaJogo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	// Grid Panels
	private static final int COLUMNS = 5;
	private static final int ROWS = 5;
	private GridPanel gridPanel_principal;
	private GridPanel gridPanel_Bonus;
	private GridPanel gridPanel_Vida;
	
	private PainelPrincipal painelPrincipal;
	private PainelBonus painelBonus;
	private PainelVida painelVida;
	// -------------
	private Thread novaThread;
	private Jogo jogo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JanelaJogo frame = new JanelaJogo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JanelaJogo() {
		setTitle("CradleOfDEI");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		contentPane = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(java.awt.Graphics g) {
				super.paintComponent(g);
				try {
					g.drawImage(ImageIO.read(JanelaJogo.class
							.getResource("/imagens/fundo/Fundo0.jpg")), 0, 0,
							this);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		gridPanel_principal = new GridPanel();
		gridPanel_principal.setShowGridLines(true);
		gridPanel_principal.setColumns(18);
		contentPane.add(gridPanel_principal, BorderLayout.CENTER);

		gridPanel_Vida = new GridPanel();
		gridPanel_Vida.setColumnSize(84);
		gridPanel_Vida.setRowSize(168);
		gridPanel_Vida.setColumns(1);
		gridPanel_Vida.setRows(1);
		gridPanel_Vida.setShowGridLines(true);
		contentPane.add(gridPanel_Vida, BorderLayout.EAST);

		//painelVida = new PainelVida(gridPanel_Vida);

		gridPanel_Bonus = new GridPanel();
		gridPanel_Bonus.setRowSize(136);
		gridPanel_Bonus.setShowGridLines(true);
		gridPanel_Bonus.setColumnSize(136);
		gridPanel_Bonus.setRows(1);
		gridPanel_Bonus.setColumns(2);
		contentPane.add(gridPanel_Bonus, BorderLayout.SOUTH);

		//painelBonus = new PainelBonus(gridPanel_Bonus);

		this.jogo = new Jogo(gridPanel_principal, gridPanel_Vida, gridPanel_Bonus);

		final Runnable iterar = new Runnable() {

			@Override
			public void run() {
				jogo.iterar();
				if (jogo.isConcluido()) {
					JOptionPane.showMessageDialog(null, "Concluido!");
					novaThread.stop();
				}
			}
		};

		novaThread = new Thread() {
			public void run() {
				while (true) {
					try {
						SwingUtilities.invokeAndWait(iterar);
						sleep(1000);
					} catch (InvocationTargetException | InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

		novaThread.start();
	}

}
