package pt.candledei;

import java.awt.event.MouseEvent;

import pt.ipleiria.estg.dei.gridpanel.GridPanel;
import pt.ipleiria.estg.dei.gridpanel.GridPanelEventHandler;

public class PainelBonus extends Iteravel implements GridPanelEventHandler{
	
	private GridPanel gridPainel_Bonus;
	private BonusMartelo bonusMartelo;
	private BonusBomba bonusBomba;

	public PainelBonus(GridPanel gridPanel_Bonus) {
		this.gridPainel_Bonus=gridPanel_Bonus;
		this.bonusMartelo = new BonusMartelo(0);
		this.bonusBomba = new BonusBomba(0);
		iniciar();
	}

	private void iniciar() {
		gridPainel_Bonus.put(0, 0,bonusMartelo.getCellRepresentation() );
		gridPainel_Bonus.put(1, 0,bonusBomba.getCellRepresentation() );
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0, int arg1, int arg2) {
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0, int arg1, int arg2) {
		
	}

	@Override
	public void mousePressed(MouseEvent arg0, int arg1, int arg2) {
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0, int arg1, int arg2) {
		
	}
	

}
