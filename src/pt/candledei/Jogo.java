package pt.candledei;

import pt.ipleiria.estg.dei.gridpanel.GridPanel;

public class Jogo extends Iteravel{

	private PainelPrincipal painelPrincipal;
	private PainelVida painelVida;
	private PainelBonus painelBonus;
	private int pontuacao;
	private boolean concluido;
	

	public Jogo(GridPanel gridPanel_Principal, GridPanel gridPanel_Vida,
			GridPanel gridPanel_Bonus) {
		this.painelPrincipal = new PainelPrincipal(gridPanel_Principal);
		this.painelBonus = new PainelBonus(gridPanel_Bonus);
		this.painelVida = new PainelVida(gridPanel_Vida);
		this.pontuacao = 0;
		this.concluido = false;
	}

	public void iterar() {
		System.out.println("itera�ao");
		painelVida.getVida();
		painelVida.iterar();
		painelPrincipal.iterar();
	}

	public boolean isConcluido() {
		return concluido;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void incrementarPontuacao(int value) {
		this.pontuacao += value;
	}
}
