package pt.candledei;

import pt.ipleiria.estg.dei.gridpanel.GridPanel;

public class PainelVida extends Iteravel {

	private GridPanel gridPanel;
	private Vida vida;
	
	public PainelVida(GridPanel gridPanel_Vida) {
		this.gridPanel= gridPanel_Vida;
		this.vida=new Vida(this);
		iniciar();
		
	}

	private void iniciar() {
		actualizar();
		
	}

	public void iterar() {
		this.vida.iterar();
	}

	public void actualizar() {
		gridPanel.put(0, 0,vida.getCellRepresentation());
		gridPanel.repaint();
		
	}

	public void getVida() {
		
	}

	
}
