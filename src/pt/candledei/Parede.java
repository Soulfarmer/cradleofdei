package pt.candledei;

import pt.ipleiria.estg.dei.gridpanel.CellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.SingleImageCellRepresentation;

public class Parede implements Celula {
	
	private String nomeImagem;
	private Posicao posicao;
	
	public Parede(Posicao posicao, PainelPrincipal painelPrincipal) {
		this.nomeImagem = "/imagens.paredes/parede.png";
		this.posicao = posicao;
	}
	
	public CellRepresentation getCellRepresentation(){
		return new SingleImageCellRepresentation(nomeImagem);
	}
	
	public Posicao getPosicao() {
		return this.posicao;
	}

}
