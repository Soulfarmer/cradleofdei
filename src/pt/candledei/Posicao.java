package pt.candledei;

public class Posicao {
	
	private int x;
	private int y;
	
	public Posicao() {
	}

	public Posicao(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}


	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public void set(Posicao posicao){
		this.x = posicao.getX();
		this.y = posicao.getY();
	}
	
	public boolean isNeighbor(Posicao destino){
		//Verificar se a posi��o destino est� a 1 bloco de distancia
		if(this.equals(destino)) return false;
		return (Math.abs(x - destino.x) == 1 && Math.abs(y - destino.y) == 0) || (Math.abs(x - destino.x) == 0 && Math.abs(y - destino.y) == 1);
	}
	
	public void somar(Posicao posicao){
		this.x += posicao.getX();
		this.y += posicao.getY();
	}
	
	@Override
	public boolean equals(Object obj) {
		Posicao pos = (Posicao) obj;
		if(this.x == pos.getX() && this.y == pos.getY())
			return true;
		return false;
	}

}
