package pt.candledei;

import java.util.LinkedList;

public class Grupo {
	private LinkedList<Bloco> blocos;
	private TipoElemento tipo;
	
	public Grupo(TipoElemento tipo){
		this.tipo = tipo;
		blocos = new LinkedList<Bloco>();
	}
	
	public void adicionar(Bloco bloco){
		bloco.setGrupo(this);
		this.blocos.add(bloco);
	}
	
	public void remover(Bloco bloco){
		this.blocos.remove(bloco);
		bloco.setGrupo(null);
	}
	
	public boolean contemBloco(Bloco bloco){
		return blocos.contains(bloco);
	}
	
	public int getNumeroBlocos(){
		return this.blocos.size();
	}

	public LinkedList<Bloco> getListaBlocos() {
		return this.blocos;
	}
	public TipoElemento getTipo(){
		return this.tipo;
	}
}
