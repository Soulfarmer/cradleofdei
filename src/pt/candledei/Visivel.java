package pt.candledei;

import pt.ipleiria.estg.dei.gridpanel.CellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.SingleImageCellRepresentation;

public class Visivel extends Iteravel {

	protected String nomeImagem;

	public Visivel() {
	}
	
	public Visivel(String nomeImagem) {
		this.nomeImagem = nomeImagem;
	}

	
	//TODO: OS METODOS ACESSORES S�O NECESSARIOS NA MODELACAO?
	public String getNomeImagem() {
		return nomeImagem;
	}

	public void setNomeImagem(String nomeImagem) {
		this.nomeImagem = nomeImagem;
	}
	
	public CellRepresentation getCellRepresentation(String nomeImagem) {
		return new SingleImageCellRepresentation(nomeImagem);
	}
	
	public CellRepresentation getCellRepresentation() {
		return new SingleImageCellRepresentation(nomeImagem);
	}

}