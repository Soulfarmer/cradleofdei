package pt.candledei;

import pt.ipleiria.estg.dei.gridpanel.CellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.SingleImageCellRepresentation;

public class BonusMartelo {

	private String nomeImagem;
	private int nivel;
	
	public BonusMartelo(int nivel) {
		super();
		this.setNivel(nivel);
		nomeImagem="/imagens/bonus/bonus_martelo/bonus_martelo_0.png";
	}

	public CellRepresentation getCellRepresentation() {
		return new SingleImageCellRepresentation(nomeImagem);
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
}