package pt.candledei;

public enum TipoElemento {
	anel(0),
	balde(1), 
	bomba(2), 
	camarao(3), 
	carne(4), 
	elmo(5), 
	folha(6), 
	madeira(7), 
	martelo(8);
	  private final int val;
	  private TipoElemento(int v) { val = v; }
	  public int getVal() { return val; }
}
