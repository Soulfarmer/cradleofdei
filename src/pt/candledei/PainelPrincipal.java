package pt.candledei;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import pt.ipleiria.estg.dei.gridpanel.GridPanel;
import pt.ipleiria.estg.dei.gridpanel.GridPanelEventHandler;
import pt.ipleiria.estg.dei.utils.FileHandler;

public class PainelPrincipal extends Iteravel implements GridPanelEventHandler {

	// FIXME: fix posicoes
	private static final Posicao[] VIZINHOS = { new Posicao(1, 0),
			new Posicao(0, 1), new Posicao(-1, 0), new Posicao(0, -1) };
	private static Integer[] posicoes = { 0, 1, 2, 3 };
	private List<Integer> listaPosicoes = Arrays.asList(posicoes);

	// TODO: Criar strings para identificar os objetos de cada nivel

	private GridPanel gridPanel;
	private LinkedList<Bloco> blocos;
	private static final String NIVEL0 = "0";
	private static final String NIVEL1 = "1";
	private static final String NIVEL2 = "2";
	private static final String BALDE = "b";
	private static final String PAREDE = "P";
	private Bloco[][] grelha;
	private Bloco bloco;
	private Posicao posicaoBloco;
	ArrayList<Bloco> blocosARebentar = new ArrayList<>();
	
	private boolean aArrastar;
	private int cadencia = 100;

	public PainelPrincipal(GridPanel gridPanel_Principal) {
		this.gridPanel = gridPanel_Principal;
		// Criar uma grelha de blocos
		this.grelha = new Bloco[18][9];
		blocos = new LinkedList<Bloco>();

		// Testes
		this.fillGridd();

		// carregarFicheiroNivel();
		this.iniciar();
		gridPanel.setEventHandler(this);
	}

	private void fillGridd() {
		for (int x = 0; x < 18; x++) {
			for (int y = 0; y < 9; y++) {
				grelha[x][y] = new Bloco(new Posicao(x, y), this);
				blocos.add(grelha[x][y]);
				grelha[x][y].setElemento(new Elemento(new Random().nextInt(9)));
			}
		}
		// TODO: criar elemento corrente
		grelha[0][0].getElemento().acorrentar();
	}

	private void carregarFicheiroNivel() {
		FileHandler handler = new FileHandler(
				"/niveis/EstadoInicialExemplo.txt");
		String conteudo = handler.readFile();
		String[] colunas = null;
		int y = 0;
		for (String linha : conteudo.split("\n")) {

			colunas = linha.split(" ");
			for (int x = 0; x < colunas.length; x++)
				if (colunas[x].substring(0, 1).equals(NIVEL0)) {
					adicionarBloco(new Bloco(new Posicao(x, y), this));
					if (colunas[x].substring(1, 2).equals(PAREDE)) {
						adicionarParede(new Parede(new Posicao(x, y), this));
					}
				}
		}
		y++;
	}

	private void adicionarParede(Parede parede) {
		// grelha[parede.getPosicao().getX()][parede.getPosicao().getY()]=parede;
	}

	private void adicionarBloco(Bloco bloco) {
		grelha[bloco.getPosicao().getX()][bloco.getPosicao().getY()] = bloco;
	}

	private void iniciar() {
		// Desenhar todos os objectos
		for (Bloco bloco : blocos) {
			this.atualizar(bloco.getPosicao());
		}
	}

	public void iterar() {
		System.out.println("Iteracao");
		if(!blocosARebentar.isEmpty()){
			for (Bloco bloco : blocosARebentar) {
				bloco.remover();
			}
			blocosARebentar.clear();
		}
			
		for (Bloco bloco : blocos) {
			bloco.iterar();
		}
		
		podeCair();
	}

	private void podeCair() {
		/*
		for (int i = grelha[0].length-1; i > 0 ; i--) {
			for (int j = grelha.length-1; j >= 0 ; j--) {
			
				if(grelha[i][j].getElemento().equals(null)){
					//vai buscar o de cima
					if(getBloco(new Posicao(i-1,j))!=null && !grelha[i-1][j].getElemento().equals(null) && !grelha[i-1][j].getElemento().isAcorrentado()){
						grelha[i][j].setElemento(grelha[i-1][j].getElemento());
						grelha[i-1][j].setElemento(null);
					}//vai buscar o da esquerda
					else if(getBloco(new Posicao(i-1,j-1))!=null && !grelha[i-1][j-1].getElemento().equals(null) && !grelha[i-1][j-1].getElemento().isAcorrentado()){
						grelha[i][j].setElemento(grelha[i-1][j-1].getElemento());
						grelha[i-1][j-1].setElemento(null);
					}//vai buscar o da direita
					else if(getBloco(new Posicao(i-1,j+1))!=null && !grelha[i-1][j+1].getElemento().equals(null) && !grelha[i-1][j+1].getElemento().isAcorrentado()){
						grelha[i][j].setElemento(grelha[i-1][j+1].getElemento());
						grelha[i-1][j+1].setElemento(null);
					}
						
				}
			}
		}
		*/
	}

	public void atualizar(Posicao posicao) {
		/*
		 * Este metodo devera ser chamado sempre que se pretenda atualizar a
		 * posi��o do gridPanel
		 */
		gridPanel.put(posicao.getX(), posicao.getY(),
				grelha[posicao.getX()][posicao.getY()].getCellRepresentation());
		gridPanel.repaint();
	}

	public boolean isPosicaoValida(Posicao posicao) {
		return posicao.getX() >= 0 && posicao.getX() < grelha.length
				&& posicao.getY() >= 0 && posicao.getY() < grelha[0].length;
	}

	private Bloco getBloco(Posicao posicaoBloco) {
		if (posicaoBloco.getX() < 0 || posicaoBloco.getX() >= grelha.length
				|| posicaoBloco.getY() < 0 || posicaoBloco.getY() >= grelha[0].length)
			return null;
		return grelha[posicaoBloco.getX()][posicaoBloco.getY()];
	}

	@Override
	public void mouseMoved(MouseEvent arg0, int arg1, int arg2) {
	}

	@Override
	public void mousePressed(MouseEvent mouseEvent, int x, int y) {
		posicaoBloco = new Posicao(x, y);
		bloco = getBloco(posicaoBloco);
		if (bloco != null) {
			if (bloco.getElemento() != null) {
				// bloco.setElemento(new
				// Corrente(bloco.getElemento().getCellRepresentation()));
				aArrastar = true;
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0, int x, int y) {
		if (aArrastar) {
			Posicao destino = new Posicao(x, y);
			// TODO: verificar se o mover forma um grupo
			if (posicaoBloco.isNeighbor(destino)
					&& getElemento(destino) != null
					&& (podeMover(getBloco(destino),bloco) || podeMover(bloco, getBloco(destino)) )) {
				bloco.mover(destino);
				this.agrupar(bloco);
				this.agrupar(getBloco(destino));
				// posicaoBloco = destino;
				aArrastar = false;
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0, int x, int y) {
		aArrastar = false;
	}

	public void moverElemento(Posicao origem, Posicao destino) {
		Elemento elemOrigem = getElemento(origem);
		Elemento elemDestino = getElemento(destino);
		setElemento(elemOrigem, destino);
		this.atualizar(destino);
		setElemento(elemDestino, origem);
		this.atualizar(origem);
	}

	private void setElemento(Elemento elemento, Posicao posicao) {
		grelha[posicao.getX()][posicao.getY()].setElemento(elemento);
	}

	public Elemento getElemento(Posicao posicao) {
		if (posicao.getX() < 0 || posicao.getX() >= grelha.length
				|| posicao.getY() < 0 || posicao.getY() >= grelha[0].length)
			return null;

		return grelha[posicao.getX()][posicao.getY()].getElemento();
	}

	public boolean podeMover(Bloco blocoDestino, Bloco blocoOrigem) {
		int contador = 0, contadorFinal = 0;
		Posicao posAux = new Posicao(0, 0);
		for (int i = 0; i < VIZINHOS.length; i++) {
			// reset do contador
			contador = 0;
			posAux.set(blocoDestino.getPosicao());
			posAux.somar(VIZINHOS[i]);
			Elemento aux = getElemento(posAux);

			// evitar procura no sentido do drag
			if (blocoOrigem.getPosicao().equals(posAux))
				continue;

			while (blocoOrigem.getElemento().equals(aux)) {
				contador++;
				posAux.somar(VIZINHOS[i]);
				aux = getElemento(posAux);
			}

			if (contador >= 2)
				return true;
			if ((blocoDestino.getPosicao().getX() == posAux.getX() || blocoDestino
					.getPosicao().getY() == posAux.getY()) && contador == 1)
				contadorFinal++;
			if (contadorFinal >= 2)
				return true;
		}
		
		return false;

		/*
		 * TODO: procurar na posi��o destino um elemento igual ao elemento do
		 * bloco 1. Saber se o destino(se � boco ou parede) existe e tem
		 * elemento [falhou] 1.1. Verificar se grupo.count >= 3 [+de 3] forma
		 * grupo e remove elementos(e baixa o nivel dos blocos todos) [-de 3]
		 * vai para outro eixo [passou] 2. Saber se o destino +1 � o elemento
		 * origem 3. Saber se o destino +1 � do mesmo tipo que o elemento origem
		 * 4. Se for todos os anteriores adiciona ao grupo e (destino+1)+1 5. Se
		 * chegar ao fim da grelha ou o tipo elemento for diferente da origem
		 * muda de eixo
		 */

		// LinkedList<Elemento> grupo = new LinkedList<Elemento>();

		// Elemento elemento =
		// grelha[destino.getX()][destino.getY()].getElemento();
	}

	public  ArrayList<Bloco> agrupar(Bloco blocoOrigem) {
		return gaita(blocoOrigem);
	}

	public ArrayList<Bloco> gaita(Bloco blocoOrigem) {
		Posicao posDestino = blocoOrigem.getPosicao();
		ArrayList<Bloco> verticais = new ArrayList<>();
		ArrayList<Bloco> horizontais = new ArrayList<>();
		for (Posicao p : VIZINHOS) {
			int count = 1;
			while (blocoOrigem.getElemento().equals(
					this.getBloco(new Posicao(posDestino.getX() + p.getX()
							* count, posDestino.getY() + p.getY() * count)).getElemento())) {
				if (p.getX() == 0) {
					verticais.add(this.getBloco(new Posicao(posDestino.getX()
							+ p.getX() * count, posDestino.getY() + p.getY()
							* count)));
				} else {
					horizontais.add(this.getBloco(new Posicao(posDestino.getX()
							+ p.getX() * count, posDestino.getY() + p.getY()
							* count)));
				}
				count++;
			}	
			if (verticais.size() >= 2) {
				blocosARebentar.addAll(verticais);
				verticais.clear();
			}
			if (horizontais.size() >= 2) {
				blocosARebentar.addAll(horizontais);
				horizontais.clear();
			}
		}
		
		
		if(!blocosARebentar.isEmpty() && blocosARebentar.get(0).getElemento().equals(blocoOrigem.getElemento()) )
			blocosARebentar.add(blocoOrigem);
		
		return blocosARebentar;
	}

}
