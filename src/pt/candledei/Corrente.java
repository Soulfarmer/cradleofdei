package pt.candledei;

import pt.ipleiria.estg.dei.gridpanel.CellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.OverlayCellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.SingleImageCellRepresentation;

//FIXME tem de ser diferente acho
public class Corrente extends Elemento {
	
	private CellRepresentation elemento;
	
	public Corrente(CellRepresentation cellRepresentation) {
		super("/imagens/correntes/corrente_simples.png");
		this.elemento  = cellRepresentation;
	}

	public CellRepresentation getCellRepresentation(){
		return new OverlayCellRepresentation(
				this.elemento,new SingleImageCellRepresentation("/imagens/correntes/corrente_simples.png"));
	}
}
