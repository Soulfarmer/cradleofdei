package pt.candledei;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import pt.ipleiria.estg.dei.gridpanel.CellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.OverlayCellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.SingleImageCellRepresentation;

public class Bloco extends Visivel implements Celula {

	private Posicao posicao;
	private int nivel;
	private PainelPrincipal painelPrincipal;
	private Elemento elemento;
	private Grupo grupo;

	public Bloco() {
		super();
	}

	public Bloco(Posicao posicao, PainelPrincipal painelPrincipal, int nivel) {
		super("/imagens/blocos/bloco" + nivel + ".png");
		this.nivel = nivel;
		this.painelPrincipal = painelPrincipal;
		this.posicao = posicao;
	}

	public Bloco(Posicao posicao, PainelPrincipal painelPrincipal) {
		this(posicao, painelPrincipal, new Random().nextInt(3));
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
		this.atualizar();
	}

	@Override
	public Posicao getPosicao() {
		return this.posicao;
	}

	public void setElemento(Elemento elemento) {
		this.elemento = elemento;
		this.atualizar();
	}

	public Elemento getElemento() {
		return this.elemento;
	}

	public void iterar() {
	}

	public void atualizar() {
		this.setNomeImagem("/imagens/blocos/bloco" + nivel + ".png");
		painelPrincipal.atualizar(this.posicao);
	}

	public CellRepresentation getCellRepresentation() {
		if (this.elemento != null)
			return new OverlayCellRepresentation(
					new SingleImageCellRepresentation(nomeImagem),
					elemento.getCellRepresentation());
		return super.getCellRepresentation();
	}

	public void mover(Posicao destino) {
		painelPrincipal.moverElemento(this.posicao, destino);
		// this.posicao = destino;
		atualizar();

	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public void remover() {
		if (this.getElemento().isAcorrentado()) {
			this.getElemento().libertarCorrente();
			return;
		}

		this.setElemento(null);

		this.nivel--;
		if (this.nivel < 0)
			this.nivel = 0;

		this.atualizar();
	}

}