package pt.candledei;

import pt.ipleiria.estg.dei.gridpanel.CellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.SingleImageCellRepresentation;

public class Vida extends Visivel {

	private static final int NIVELMAX = 20;
	private PainelVida painelVida;
	private int nivel;

	public Vida(PainelVida painelVida) {
		super("/imagens/vida/vida_");
		this.nivel = NIVELMAX;
		this.painelVida = painelVida;
	}

	@Override
	public CellRepresentation getCellRepresentation() {
		return new SingleImageCellRepresentation(this.nomeImagem + nivel + ".png");
	}

	public void iterar() {
		if(nivel==0){
			nivel=20;
		}else{
			nivel--;
		}
		actualizar();

	}

	private void actualizar() {
		painelVida.actualizar();

	}

}
