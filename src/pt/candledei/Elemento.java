package pt.candledei;

import pt.ipleiria.estg.dei.gridpanel.CellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.OverlayCellRepresentation;
import pt.ipleiria.estg.dei.gridpanel.SingleImageCellRepresentation;

public class Elemento extends Visivel {

	private String correnteImagem = "/imagens/correntes/corrente_simples.png";
	private boolean temCorrente = false;

	// Temporary variables
	protected static final String PATH = "/imagens/elementos";
	private static String[] elementNames = { "/anel", "/balde", "/bomba",
			"/camarao", "/carne", "/elmo", "/folha", "/madeira", "/martelo" };
	private static String extension = ".png";

	public Elemento() {
	}

	public Elemento(int index) {
		this(PATH + elementNames[index] + extension);
	}

	public Elemento(String nomeImagem) {
		super(nomeImagem);
	}

	public void libertarCorrente() {
		this.temCorrente = false;
	};

	public void acorrentar() {
		this.temCorrente = true;
	}

	public boolean isAcorrentado() {
		return this.temCorrente;
	}

	@Override
	public CellRepresentation getCellRepresentation() {
		if (temCorrente)
			return new OverlayCellRepresentation(
					super.getCellRepresentation(nomeImagem),
					new SingleImageCellRepresentation(correnteImagem));
		else
			return super.getCellRepresentation(nomeImagem);
	}

	@Override
	public boolean equals(Object obj) {
		Elemento elem = (Elemento) obj;
		if (elem == null)
			return false;
		return this.nomeImagem.equals(elem.nomeImagem);
	}

}