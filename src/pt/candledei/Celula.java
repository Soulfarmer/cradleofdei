package pt.candledei;

import pt.ipleiria.estg.dei.gridpanel.CellRepresentation;

public interface Celula {

	public abstract CellRepresentation getCellRepresentation();

	public Posicao getPosicao();

}